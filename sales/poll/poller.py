import django
import os
import sys
import time
import json
import requests
from django.http import JsonResponse

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here.
# from sales_rest.models import Something

from sales_rest.models import AutomobileVO, VehicleModelVO, ManufacturerVO, ImageVO


def get_automobiles():
    response = requests.get("http://inventory-api:8000/api/automobiles")
    content = json.loads(response.content)
    for automobile in content["autos"]:
        try:
            model_id = automobile["model"]["id"]
            vehicle_model_vo = VehicleModelVO.objects.get(id=model_id)
        except VehicleModelVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid VehicleModelVO id"},
                status=404
            )
        AutomobileVO.objects.update_or_create(
            id=automobile["id"],
            defaults={
                "vin": automobile["vin"],
                "model": vehicle_model_vo,
            },
        )


def get_vehicle_models():
    response = requests.get("http://inventory-api:8000/api/models")
    content = json.loads(response.content)
    for vehicle in content["models"]:
        try:
            manufacturer_id = vehicle["manufacturer"]["id"]
            manufacturer_vo = ManufacturerVO.objects.get(id=manufacturer_id)
        except ManufacturerVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid ManufacturerVO id"},
                status=404
            )
        VehicleModelVO.objects.update_or_create(
            id=vehicle["id"],
            defaults={
                "name": vehicle["name"],
                "picture_url": vehicle["picture_url"],
                "manufacturer": manufacturer_vo,
            },
        )


def get_manufacturers():
    response = requests.get("http://inventory-api:8000/api/manufacturers")
    content = json.loads(response.content)
    for manufacturer in content["manufacturers"]:
        ManufacturerVO.objects.update_or_create(
            id=manufacturer["id"],
            defaults={
                "name": manufacturer["name"],
            },
        )


def get_images():
    response = requests.get("http://inventory-api:8000/api/images")
    content = json.loads(response.content)
    for image in content["images"]:
        try:
            model_id = image["model"]["id"]
            vehicle_model_vo = VehicleModelVO.objects.get(id=model_id)
        except VehicleModelVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid VehicleModelVO id"},
                status=404
            )
        ImageVO.objects.update_or_create(
            id=image["id"],
            defaults={
                "picture_url": image["picture_url"],
                "model": vehicle_model_vo,
            },
        )
    

def poll():
    while True:
        print('Sales poller polling for data - IT IS WORKING!')
        try:
            get_automobiles()
            get_vehicle_models()
            get_manufacturers()
            get_images()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(10)


if __name__ == "__main__":
    poll()
