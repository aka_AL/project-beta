from common.json import ModelEncoder
from .models import SalesPerson, Customer, Sale, AutomobileVO, VehicleModelVO, ManufacturerVO, ImageVO

class ManufacturerVODetailEncoder(ModelEncoder):
    model = ManufacturerVO
    properties = ["id", "name"]


class VehicleModelVODetailEncoder(ModelEncoder):
    model = VehicleModelVO
    properties = ["id", "name", "picture_url", "manufacturer"]
    encoders = {
        "manufacturer": ManufacturerVODetailEncoder(),
    }


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["id", "vin", "model"]
    encoders = {
        "model": VehicleModelVODetailEncoder(),
    }


class ImageVOEncoder(ModelEncoder):
    model = ImageVO
    properties = ["id", "picture_url", "model"]
    encoders = {
        "model": VehicleModelVODetailEncoder(),
    }


class SalesPeopleListEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["id", "name", "employee_number"]


class SalesPersonDetailEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["id", "name", "employee_number"]


class CustomersListEncoder(ModelEncoder):
    model = Customer
    properties = ["id", "name", "address", "phone"]


class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = ["id", "name", "address", "phone"]


class SalesListEncoder(ModelEncoder):
    model = Sale
    properties = ["id", "automobile", "sales_person", "customer", "price"]
    encoders = {
        "automobile": AutomobileVODetailEncoder(),
        "sales_person": SalesPersonDetailEncoder(),
        "customer": CustomerDetailEncoder(),
    }

class SaleDetailEncoder(ModelEncoder):
    model = Sale
    properties = ["id", "automobile", "sales_person", "customer", "price"]
    encoders = {
        "automobile": AutomobileVODetailEncoder(),
        "sales_person": SalesPersonDetailEncoder(),
        "customer": CustomerDetailEncoder(),
    }