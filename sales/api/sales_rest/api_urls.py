from django.urls import path
from .api_views import api_list_sales_people, api_show_sales_person, api_show_customer, api_list_customers, api_list_sales, api_show_sale, api_list_unsold_autos

urlpatterns = [
    path("sales_people/", api_list_sales_people, name="api_list_sales_people"),
    path("sales_people/<int:pk>/", api_show_sales_person, name="api_show_sales_person"),
    path("customers/", api_list_customers, name="api_list_customers"),
    path("customers/<int:pk>/", api_show_customer, name="api_show_customer"),
    path("sales/", api_list_sales, name="api_list_sales"),
    path("sales/<int:pk>/", api_show_sale, name="api_show_sale"),
    path("automobiles/unsold/", api_list_unsold_autos, name="api_list_unsold_autos"),
]