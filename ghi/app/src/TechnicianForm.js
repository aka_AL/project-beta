import React from 'react'
import { Link } from 'react-router-dom'


async function loadTechnicians () {
    const response = await fetch ("http://localhost:8080/api/technicians/")
    if (response.ok) {
        const data = await response.json()
    }
}
loadTechnicians()

// async function technicianSubmit(technician) {
    
// }

class CreateTechnician extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            employee_id: ''
        }

        this.handleNameChange = this.handleNameChange.bind(this)
        this.handleIDChange = this.handleIDChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }

    handleIDChange(event) {
        const value = event.target.value;
        this.setState({employee_id: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state}

        const technicianUrl = "http://localhost:8080/api/technicians/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(technicianUrl, fetchConfig)
        if (response.ok) {
            const newTechnician = await response.json()
            console.log(newTechnician)
        
            const cleared = {
                name: '',
                employee_id: ''
            }
            this.setState(cleared)
        }
    }

    // async componentDidMount() {
    //     const url = "http://localhost:8080/api/technicians/"
    //     const response = await fetch(url)

    //     if (response.ok) {
    //         const data = await response.json()
    //         console.log("mount", data)
    //         this.setState({technician: data.technician})
    //     }
    // }

    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a New Technician</h1>
                <form onSubmit={this.handleSubmit} id="create-technician-form">
                    <div className="form-floating mb-3">
                        <input onChange={this.handleNameChange} value={this.state.name} placeholder="name" name="name" required type="text" id="name" className="form-control" />
                        <label htmlFor="name">Full Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={this.handleIDChange} value={this.state.employee_id} placeholder="employee_id" name="employee_id" required type="text" id="employee_id" className="form-control" />
                        <label htmlFor="employee_id">Employee ID Number</label>
                    </div>
                    <div>
                    <button className="btn btn-primary" style={{backgroundColor: '#308454', border: 'none'}}>Create</button>
                    </div>
                </form>
            </div>
            </div>
            </div>
        )
    }
}


export default CreateTechnician
    