import React from 'react';
import { Link } from 'react-router-dom';


class ManufacturersList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            manufacturers: [],
        }
    }


    async componentDidMount() {
        const resp = await fetch('http://localhost:8100/api/manufacturers/');
        if (resp.ok) {
            const data = await resp.json();
            this.setState({
                manufacturers: data.manufacturers
            })
        } else {
            console.log("There was an error fetching list of manufacturers: ", resp);
        }
    }
    
    
    render() {
        return(
            <React.StrictMode>
            <div>
                <br />
                <h1>Manufacturers</h1>
                <br />
                <Link to="/manufacturers/new" className="btn btn-primary btn-lg px-4 gap-3" style={{backgroundColor: '#308454', border: 'none'}}>Create New Manufacturer</Link>
                <br />
                <br />
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.state.manufacturers.map(manufacturer => {
                        return (
                        <tr key={manufacturer.id}>
                            <td>{manufacturer.name}</td>
                        </tr>
                        );
                    })}
                    </tbody>
                </table>
                <br />
            </div>
            </React.StrictMode>
        );
    }
}

export default ManufacturersList;