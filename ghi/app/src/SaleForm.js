import React from 'react';
import { Link } from 'react-router-dom';


class SaleForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            automobiles: [],
            salesPeople: [],
            customers: [],
            price: 0,
        }
    }


    async componentDidMount() {
        const automobilesResp = await fetch('http://localhost:8090/api/automobiles/unsold/');
        if (automobilesResp.ok) {
            const data = await automobilesResp.json();
            this.setState({
                automobiles: data.automobiles
            });
        }

        const salesPeopleResp = await fetch('http://localhost:8090/api/sales_people/');
        if (salesPeopleResp.ok) {
            const data = await salesPeopleResp.json();
            this.setState({
                salesPeople: data.sales_people
            });
        }
        
        const customersResp = await fetch('http://localhost:8090/api/customers/');
        if (customersResp.ok) {
            const data = await customersResp.json();
            this.setState({
                customers: data.customers
            });
        }
    }


    handleChange = event => {
        const {name, value} = event.target;
        this.setState({
            [name]: value
        });
    }


    handleSubmit = async event => {
        event.preventDefault();
        const data = {...this.state};
        data.sales_person = data.salesPerson;
        delete data.automobiles;
        delete data.salesPeople;
        delete data.customers;
        delete data.salesPerson;

        const resp = await fetch ('http://localhost:8090/api/sales/', {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        if (resp.ok) {
            const newSale = await resp.json();
            window.location.href = "/sales";
        }
    }


    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <br />
                    <Link to="/sales" className="btn btn-primary btn-lg px-4 gap-3" style={{backgroundColor: '#308454', border: 'none'}}>Back</Link>
                    <div className="shadow p-4 mt-4">
                        <h1>Record a New Sale</h1>
                        <form onSubmit={this.handleSubmit} id="create-sale-form">
                        <div className="mb-3">
                            <select onChange={this.handleChange} value={this.state.automobile} required name="automobile" id="automobile" className="form-select">
                                <option value="">Choose an Automobile</option>
                                {
                                    this.state.automobiles.map(automobile => {
                                        return <option key={automobile.vin} value={automobile.vin}>{automobile.model.manufacturer.name} {automobile.model.name} (VIN: {automobile.vin})</option>
                                    })
                                }
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={this.handleChange} value={this.state.salesPerson} required name="salesPerson" id="salesPerson" className="form-select">
                                <option value="">Choose a Sales Person</option>
                                {
                                    this.state.salesPeople.map(salesPerson => {
                                        return <option key={salesPerson.id} value={salesPerson.id}>{salesPerson.name}</option>
                                    })
                                }
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={this.handleChange} value={this.state.customer} required name="customer" id="customer" className="form-select">
                                <option value="">Choose a Customer</option>
                                {
                                    this.state.customers.map(customer => {
                                        return <option key={customer.id} value={customer.id}>{customer.name}</option>
                                    })
                                }
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChange} value={this.state.price} placeholder="price" required type="number" step="1" name="price" id="price" className="form-control"/>
                            <label htmlFor="price">Sale Price</label>
                        </div>
                        <button className="btn btn-primary" style={{backgroundColor: '#308454', border: 'none'}}>Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default SaleForm;