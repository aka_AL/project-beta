import React from 'react';
import { Link } from 'react-router-dom';


class AutomobileForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            color: "",
            year: 0,
            vin: "",
            vehicleModel: "",
            vehicleModels: [],
        }
    }


    async componentDidMount() {
        const resp = await fetch('http://localhost:8100/api/models/');
        if (resp.ok) {
            const data = await resp.json();
            this.setState({
                vehicleModels: data.models
            });
        }
    }


    handleChange = event => {
        const {name, value} = event.target;
        this.setState({
            [name]: value
        });
    }


    handleSubmit = async event => {
        event.preventDefault();
        const data = {...this.state};
        data.model_id = data.vehicleModel
        delete data.vehicleModel;
        delete data.vehicleModels;
        console.log("HERE IS MY DATA", data)

        const resp = await fetch ('http://localhost:8100/api/automobiles/', {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        if (resp.ok) {
            const newAutomobile = await resp.json();
            window.location.href = "/automobiles";
        }
    }


    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <br />
                    <Link to="/automobiles" className="btn btn-primary btn-lg px-4 gap-3" style={{backgroundColor: '#308454', border: 'none'}}>Back</Link>
                    <div className="shadow p-4 mt-4">
                        <h1>Create an Automobile</h1>
                        <form onSubmit={this.handleSubmit} id="create-automobile-form">
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChange} value={this.state.color} placeholder="color" required type="text" name="color" id="color" className="form-control"/>
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChange} value={this.state.year} placeholder="year" required type="text" name="year" id="year" className="form-control"/>
                            <label htmlFor="year">Year</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChange} value={this.state.vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control"/>
                            <label htmlFor="vin">VIN</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={this.handleChange} value={this.state.vehicleModel} required name="vehicleModel" id="vehicleModel" className="form-select">
                                <option value="">Choose a Vehicle Model</option>
                                {
                                    this.state.vehicleModels.map(model => {
                                        return <option key={model.id} value={model.id}>{model.name}</option>
                                    })
                                }
                            </select>
                        </div>
                        <button className="btn btn-primary" style={{backgroundColor: '#308454', border: 'none'}}>Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default AutomobileForm;