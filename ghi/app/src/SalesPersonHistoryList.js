import React from 'react';
import { resolvePath } from 'react-router-dom';

class SalesPersonHistoryList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            salesPeople: [],
            totalSales: [],
            tempSales: [],
        }
    }


    async componentDidMount() {
        const salesPeopleResp = await fetch('http://localhost:8090/api/sales_people/');
        if (salesPeopleResp.ok) {
            const data = await salesPeopleResp.json();
            this.setState({
                salesPeople: data.sales_people,
            })
        } else {
            console.log("There was an error fetching list of sales people: ", salesPeopleResp);
        }

        const salesHistoryResp = await fetch ('http://localhost:8090/api/sales/');
        if (salesHistoryResp.ok) {
            const data = await salesHistoryResp.json();
            this.setState({
                totalSales: data.sales,
                tempSales: data.sales
            });
        } else {
            console.log("There was an error fetching sales history: ", salesHistoryResp);
        }
    }


    handleChange = async event => {
        const value = event.target.value;
        
        const salesList = [];
        if (value === "") {
            this.setState({
                tempSales: this.state.totalSales
            });
        } else {
            for (const sale of this.state.totalSales) {
                if (sale.sales_person.id.toString() === value) {
                    salesList.push(sale);
                }
            }
            this.setState({
                tempSales: salesList
            });
        }
    }


    render() {
        return(
            <React.StrictMode>
            <div>
                <br />
                <h1>Sales Person History</h1>
                <br />
                <select onChange={this.handleChange} name="sales_person" id="sales_person" className="form-select">
                    <option value="">Filter by Sales Person</option>
                    {
                        this.state.salesPeople.map(salesPerson => {
                            return <option key={salesPerson.id} value={salesPerson.id}>{salesPerson.name}</option>
                        })
                    }
                </select>
                <br />
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Sales Person</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Sales Price</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.state.tempSales.map(sale => {
                        return (
                        <tr key={sale.id}>
                            <td>{sale.sales_person.name}</td>
                            <td>{sale.customer.name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>${sale.price.toLocaleString()}</td>
                        </tr>
                        );
                    })}
                    </tbody>
                </table>
            </div>
            </React.StrictMode>
        );
    }
}

export default SalesPersonHistoryList;