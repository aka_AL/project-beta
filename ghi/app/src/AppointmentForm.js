import React from "react";
import { Link } from 'react-router-dom';

class AppointmentForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      customer_name: "",
      vin: "",
      reason: "",
      technician: "",
      technicians: [],
    };

    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleVinChange = this.handleVinChange.bind(this);
    this.handleTechnicianChange = this.handleTechnicianChange.bind(this);
    this.handleReasonChange = this.handleReasonChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    const url = "http://localhost:8080/api/technicians/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ technicians: data });
    }
  }

  handleNameChange(event) {
    const value = event.target.value;
    this.setState({ customer_name: value });
  }

  handleVinChange(event) {
    const value = event.target.value;
    this.setState({ vin: value });
  }

  handleTechnicianChange(event) {
    const value = event.target.value;
    this.setState({ technician: value });
  }

  handleReasonChange(event) {
    const value = event.target.value;
    this.setState({ reason: value });
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state }; //error
    delete data.technicians;

    const appointmentUrl = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
      const newAppointment = await response.json();

      const cleared = {
        customer_name: "",
        vin: "",
        technician: "",
        reason: "",
        technicians: [],
      };
      this.setState(cleared);
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <br />
          <Link to="/appointments" className="btn btn-primary btn-lg px-4 gap-3" style={{backgroundColor: '#308454', border: 'none'}}>Back</Link>
          <div className="shadow p-4 mt-4">
            <h1>Create a New Appointment</h1>
            <form onSubmit={this.handleSubmit} id="create-appointment-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleNameChange}
                  value={this.state.customer_name}
                  placeholder="customer_name"
                  name="customer_name"
                  required
                  type="text"
                  id="customer_name"
                  className="form-control"
                />
                <label htmlFor="customer_name">Customer Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleVinChange}
                  value={this.state.vin}
                  placeholder="vin"
                  name="vin"
                  required
                  type="text"
                  id="vin"
                  className="form-control"
                />
                <label htmlFor="vin">VIN</label>
              </div>
              <div className="form-floating mb-3">
                <select
                  onChange={this.handleTechnicianChange}
                  value={this.state.technician}
                  placeholder="technician"
                  required
                  type="text"
                  name="technician"
                  id="technician"
                  className="form-select"
                >
                  <option value="">Choose a Technician</option>
                  {this.state.technicians.map((technician) => {
                    return (
                      <option
                        value={technician.employee_id}
                        key={technician.employee_id}
                      >
                        {technician.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleReasonChange}
                  value={this.state.reason}
                  placeholder="reason"
                  required
                  type="text"
                  name="reason"
                  id="reason"
                  className="form-control"
                />
                <label htmlFor="reason">Reason</label>
              </div>
              <button className="btn btn-primary" style={{backgroundColor: '#308454', border: 'none'}}>Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default AppointmentForm;
