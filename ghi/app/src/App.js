import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './TechnicianForm'
import SaleForm from './SaleForm';
import SalesList from './SalesList';
import AppointmentForm from './AppointmentForm'
import AppointmentList from './AppointmentList'
import SalesPersonHistoryList from './SalesPersonHistoryList';
import ManufacturersList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';
import VehicleModelsList from './VehicleModelsList';
import VehicleModelForm from './VehicleModelForm';
import AutomobilesList from './AutomobilesList';
import AutomobileForm from './AutomobileForm';
import ServiceList from './Service-historyList';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/technicians/new/" element={<TechnicianForm />} />
          <Route path="/appointments" element={<AppointmentList />} />
          <Route path="/appointments/new/" element={<AppointmentForm />} />
          <Route path="/service-history" element={<ServiceList />} />
          <Route path="/sales/" element={<SalesList />} />
          <Route path="/sales/new" element={<SaleForm />} />
          <Route path="/sales-person-history" element={<SalesPersonHistoryList />} />
          <Route path="/manufacturers" element={<ManufacturersList />} />
          <Route path="/manufacturers/new" element={<ManufacturerForm />} />
          <Route path="/vehicle-models" element={<VehicleModelsList />} />
          <Route path="/vehicle-models/new" element={<VehicleModelForm />} />
          <Route path="/automobiles" element={<AutomobilesList />} />
          <Route path="/automobiles/new" element={<AutomobileForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
